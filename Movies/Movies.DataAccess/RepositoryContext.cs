﻿using System;
using Microsoft.EntityFrameworkCore;
using Movies.Models.Entities;

namespace Movies.DataAccess
{
    public class RepositoryContext : DbContext
    {
        // Default constructor to initialize connection to database
        public RepositoryContext(DbContextOptions<RepositoryContext> options) : base(options)
        {
        }

        /// <summary>
        /// Provides metadata for database mapping
        /// </summary>
        /// <param name="builder">Interface to provide metadata</param>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            // Sets "Title" of movie as a unique key via index
            builder.Entity<Movie>()
                   .HasIndex(m => m.Title)
                   .IsUnique();
        }

        /// <summary>
        /// Movies repository
        /// </summary>
        public DbSet<Movie> Movie { get; set; }
    }
}
