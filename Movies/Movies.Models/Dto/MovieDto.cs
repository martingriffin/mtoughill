﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Movies.Models.Dto
{
    public class MovieDto
    {
        /// <summary>
        /// Title of the movie
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string Title { get; set; }

        /// <summary>
        /// True if the user likes the movie and false if the user does not like the movie
        /// </summary>
        public bool IsThumbsUp { get; set; }
    }
}
