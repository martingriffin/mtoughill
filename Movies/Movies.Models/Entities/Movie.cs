﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Movies.Models.Entities
{
    public class Movie
    {
        /// <summary>
        /// Primary key, naming convention makes it assume primary key
        /// </summary>
        [Key]
        [System.ComponentModel.DataAnnotations.Schema.DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Title of the movie
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string Title { get; set; }

        /// <summary>
        /// Number of thumbs up for this movie
        /// </summary>
        [Range(0, Int64.MaxValue)]
        public long UpVotes { get; set; }

        /// <summary>
        /// Number of thumbs down for this movie
        /// </summary>
        [Range(0, Int64.MaxValue)]
        public long DownVotes { get; set; }
    }
}
