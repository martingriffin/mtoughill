﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Movies.DataAccess;

namespace Movies.API
{
    public class Startup
    {
        private const string DEFAULT_CONNECTION = "DefaultConnection";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options => options.AddPolicy("AllowAll", p =>
                                                          p.AllowAnyOrigin()
                                                          .AllowAnyMethod()
                                                          .AllowAnyHeader()));
            
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // Registers in IoC container with "Scope" lifetime meaning new context
            // object is injected on a web request to make it feel like a "unit of work"
            services.AddDbContext<RepositoryContext>(options =>
                options.UseMySql(Configuration.GetConnectionString(DEFAULT_CONNECTION)
            ));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Perform database migration
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<RepositoryContext> ();
                context.Database.Migrate();
            }

            app.UseCors("AllowAll");

            app.UseMvc();
        }
    }
}
