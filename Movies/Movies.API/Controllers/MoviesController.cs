﻿using KeyLocks;
using Microsoft.AspNetCore.Mvc;
using Movies.DataAccess;
using Movies.Models.Dto;
using Movies.Models.Entities;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Movies.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly RepositoryContext _context;
        private static KeyLock<int> _keyLock = new KeyLock<int>();

        public MoviesController(RepositoryContext context)
        {
            _context = context;
        }

        // POST api/movies
        [HttpPost]
        [ProducesResponseType(200, Type = typeof(Movie))]
        [ProducesResponseType(201, Type = typeof(Movie))]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Movie>> Post([FromBody] MovieDto value)
        {
            // This does return data even though the frontend is not going to do anything with the data
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            // Check if the data exists already, case sensitive since this is movie titles
            Movie result = _context.Movie.Where(x => x.Title == value.Title).FirstOrDefault();
            if (result == null)
            {
                // Create movie
                int thumbsUpCount = 0;
                int thumbsDownCount = 0;
                if (value.IsThumbsUp == true)
                {
                    thumbsUpCount++;
                }
                else
                {
                    thumbsDownCount++;
                }

                result = new Movie()
                {
                    Title = value.Title,
                    UpVotes = thumbsUpCount,
                    DownVotes = thumbsDownCount
                };
                _context.Movie.Add(result);

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    return StatusCode((int)HttpStatusCode.InternalServerError, e.GetBaseException().Message);
                }
                return CreatedAtAction(nameof(Post), result);
            }

            try
            {
                // Adding lock here to prevent multiple users updating counts at the same time
                await _keyLock.RunWithLock(result.Id, async () =>
                {
                    // Update movie, doesn't need to add to context as the earlier retrieval puts it in context
                    if (value.IsThumbsUp == true)
                    {
                        result.UpVotes++;
                    }
                    else
                    {
                        result.DownVotes++;
                    }
                    await _context.SaveChangesAsync();
                });
            }
            catch (Exception e)
            {
                return StatusCode((int) HttpStatusCode.InternalServerError, e.GetBaseException().Message);
            }
            finally
            {
                _keyLock.RemoveLock(result.Id);
            }
            return Ok(result);
        }
    }
}
