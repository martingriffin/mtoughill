import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ApiService } from '../api.service';
import { Vote } from '../models/vote';

@Component({
  selector: 'voting',
  templateUrl: './voting.component.html',
  styleUrls: ['./voting.component.css']
})
export class VotingComponent  {
    @Input("movieTitle")
    title: string;

    @Output()
    refresh = new EventEmitter();

    @Output()
    refreshEmpty = new EventEmitter();

    constructor(private  apiService:  ApiService) {}
    
    private thumbsUp(): void {
        const vote: Vote = new Vote();
        vote.IsThumbsUp = true;
        vote.Title = this.title;
        this.apiService.setVote(vote).subscribe(
            result => {
                if (result != null) {
                    this.refresh.emit();
                } else {
                    this.refreshEmpty.emit();
                }
            }
        );
    }

    private thumbsDown(): void {
        const vote: Vote = new Vote();
        vote.IsThumbsUp = false;
        vote.Title = this.title;
        this.apiService.setVote(vote).subscribe();
        this.refresh.emit();
    }
}
