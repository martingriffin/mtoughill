import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from  '@angular/common/http';
import { Observable, of, pipe } from 'rxjs';
import { Movies } from './models/movies';
import { map, catchError } from 'rxjs/operators';
import { Movie } from './models/movie';
import { Vote } from './models/vote';

// Makes the sevice available to all and a singleton
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private static API_URL: string  =  'https://api.themoviedb.org/3/discover/movie?api_key=9119680678eb89bda7f492416d71bd4d&primary_release_year=2018&page=';
  private static VOTING_URL: string = 'http://localhost:5000/api/movies'
  private static MIN_PAGES: number = 1;
  private static MAX_PAGES: number = 20;
    
  private static HTTP_OTPIONS = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
  }


  constructor(private  httpClient:  HttpClient) {}

  getMovie(): Observable<Movie>{
    const page: number = this.getRandomValue(ApiService.MIN_PAGES, ApiService.MAX_PAGES);
    return this.httpClient.get<Movies>(`${ApiService.API_URL}` + page)
      .pipe(map(response => this.getRandomMovie(response)))
      .pipe(catchError(this.handleError<Movie>('getMovie', new Movie())));
  }

  setVote(vote: Vote): Observable<Vote> {
    return this.httpClient.post<Vote>(ApiService.VOTING_URL, vote, ApiService.HTTP_OTPIONS)
      .pipe(catchError(this.handleError<Vote>('setVote')));
  }

  private getRandomValue(min: number, max: number) : number {
    let result: number = Math.floor(Math.random() * (max - min + 1) + min)
    return result;
  }

  private getRandomMovie(movies: Movies): Movie {
    return movies.results[this.getRandomValue(1, movies.results.length - 1)];
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(`${operation} failed: ${error.message}`);
   
      // Return empty for now
      return of(result as T);
    };
  }
}
