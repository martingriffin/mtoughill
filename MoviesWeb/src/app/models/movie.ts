export class Movie {
    title: string;
    vote_average: number;
    poster_path: string;
}
  