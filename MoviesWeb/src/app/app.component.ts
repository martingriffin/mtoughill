import { Component, OnInit } from '@angular/core';
import { ApiService } from './api.service';
import { Movie } from './models/movie';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string = '';
  imageUrl: string = '';
  avgRating: number = 0;
  private static IMAGE_URL: string = 'http://image.tmdb.org/t/p/w154/';

  constructor(private  apiService:  ApiService) {}

  ngOnInit() {
    this.loadMovie();
  }

  private loadMovie(): void {
    this.apiService.getMovie().subscribe((data: Movie) => {
      this.title = data.title;
      if (data.poster_path != null) {
        this.imageUrl = AppComponent.IMAGE_URL + data.poster_path;
      } else {
        this.imageUrl = null;
      }
      this.avgRating = data.vote_average;
    });
  }

  private loadEmptyMovie(): void {
    this.title = null;
  }
}
